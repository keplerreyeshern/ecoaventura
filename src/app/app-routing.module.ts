import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from "./pages/main/main.component";
import { ContactComponent } from "./pages/contact/contact.component";
import { AboutComponent } from "./pages/about/about.component";
import { PrivacyComponent } from "./pages/privacy/privacy.component";
import {PoliticsComponent} from "./pages/politics/politics.component";

const routes: Routes = [
  {
    path: '',
    component: MainComponent
  },
  {
    path: 'contacto',
    component: ContactComponent
  },
  {
    path: 'quienes-somos',
    component: AboutComponent
  },
  {
    path: 'politicas',
    component: PoliticsComponent
  }
  ,
  {
    path: 'aviso-privacidad',
    component: PrivacyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    initialNavigation: 'enabled'
})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
